# nats-gateway #


a POC for nats gateway configuration

# overview

* given two sites : S1 , S2 each having a nats server  N1 ,N2

* given two agents: 

    * one subscriber: "subscriber" connected to N1 without an account

    * one producer : "publisher" connected to N2 with an account


we want subscriber subscribes  (on N1) to subject s "publisher" ( on N2 )

subjects are  snifferlog.>


# steps

set up N1 on S1 and create the subscriber

set up N2 on S2 , create an account

create publisher and connect it to N2 via the account

establishe a gateway between N1 and N2

authorize anyone to subscribe to snifferlog.>


# biblio

## secure nats connections
https://itnext.io/secure-pub-sub-with-nats-fcda983d0612


