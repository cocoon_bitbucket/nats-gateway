package main

import (
	natsgateway "bitbucket.org/cocoon_bitbucket/natsgateway/pkg"
	"flag"
)

var (
	topic  = "sniffer"
	server = "nats://127.0.0.1:4222"
)

var caCert = flag.String("ca", "", "CA certificate for tls connection")

func main() {

	flag.Parse()

	natsgateway.PublisherTls(topic, server, *caCert)

}
