

# for Running a NATS server with TLS

see: https://itnext.io/secure-pub-sub-with-nats-fcda983d0612

## steps 

### Creating the server certificate

tls.GenCerticate generates self signed cert.pem and key.pem


### create a server.conf

    listen: 0.0.0.0:4222
    tls: {
      cert_file: "./cert.pem"
      key_file: "./key.pem"
    }
    
launch nats with

    nats-server -c server.conf
    
    