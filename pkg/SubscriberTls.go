package natsgateway

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"encoding/json"
	"fmt"
	nats "github.com/nats-io/nats.go"
	"log"
	"os"
	"os/signal"
)

/*

	a tls nats subscriber




*/

type Message struct {
	Count   int
	Message string
}

func (m *Message) Next() {
	m.Count += 1
	m.Message = fmt.Sprintf("My message number %d", m.Count)
}

func (m *Message) Marshal() (data []byte, err error) {
	return json.Marshal(&m)
}

func SubscriberTls(subject, server string, caCert string, user string) {

	// After setting everything up!
	// Wait for a SIGINT (perhaps triggered by user with CTRL-C)
	// Run cleanup when signal is received
	signalChan := make(chan os.Signal, 1)
	cleanupDone := make(chan struct{})
	signal.Notify(signalChan, os.Interrupt)

	var err error
	var pub *publisher.NatsPublisher

	if caCert == "" {
		pub, err = publisher.NewNatsPublisher(subject, server)
	} else {
		pub, err = publisher.NewNatsTlsPublisher(subject, server, caCert, user)
	}
	if err != nil {
		log.Printf("nats Listener failed :%s\n", err.Error())
		return
	}
	defer pub.Close()
	log.Printf("listener : connected to nats server : %s\n", server)

	// subscribe to all inputs
	topic := subject + ".>"
	topic = ">"
	log.Printf("listener : set subscribe topic to: %s\n", topic)
	counter := 0
	_, _ = pub.Conn.Subscribe(topic, func(m *nats.Msg) {
		//log.Printf("Listener catch a message: %s on  subject: %s\n", string(m.Data), m.Subject)
		log.Printf("Listener : caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")
		counter += 1
	})

	log.Printf("listener : starting subcribing %s at %s\n", topic, server)

	// wait for CTRL+C
	<-signalChan

	// leaving
	log.Printf("Listener have received: %d", counter)
	pub.Close()
	close(cleanupDone)

	// wait for cleanup
	<-cleanupDone

}
