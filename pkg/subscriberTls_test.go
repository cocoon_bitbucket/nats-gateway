package natsgateway

import (
	"testing"
)

func TestTlsSubscriber(t *testing.T) {

	topic := "sniffer"
	server := "tls://127.0.0.1:4222"
	//server := "tls://nats.demo.io:4443"
	ca := "./cert.pem"
	//userJWT := "/Users/cocoon/.nkeys/creds/op/admin/admin.creds"
	userJWT := "/Users/cocoon/.nkeys/creds/op/ines/ines.creds"

	SubscriberTls(topic, server, ca, userJWT)

}

//// tls as a scheme will enable secure connections by default. This will also verify the server name.
////nc, err := nats.Connect(server)
//nc, err := nats.Connect(server, nats.RootCAs(ca))
//if err != nil {
//	log.Fatalf("cannot connect to %s :%s\n",server,err)
//
//}
//
//
//nc.Subscribe( ">", func(m *nats.Msg) {
//	//log.Printf("Listener catch a message: %s on  subject: %s\n", string(m.Data), m.Subject)
//	log.Printf("Listener : caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")
//})
//
//message := " hello there "
//nc.Publish("test", []byte(message))
//
//
//
//select {}
//
//
