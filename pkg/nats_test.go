package natsgateway

import (
	nats "github.com/nats-io/nats.go"
	"log"
	"testing"
)

var server = "nats:127.0.0.1:4222"
var userJWT = "~/.nkeys/creds/op/ines/ines.creds"
var ca = "cert.pem"

func TestNatsConnectionWithUser(t *testing.T) {

	cert := nats.RootCAs(ca)
	creds := nats.UserCredentials(userJWT)

	nc, err := nats.Connect(server, cert, creds)
	if err != nil {
		log.Fatal(err.Error())
	}

	nc.Close()

}
