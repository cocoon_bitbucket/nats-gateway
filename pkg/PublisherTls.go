//package natsgateway
package natsgateway

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"log"
	"time"
)

func PublisherTls(topic string, server string, caCert string) {

	var delay = 1 * time.Second
	//var caCert = "./cert.pem"

	var userCreds = "/Users/cocoon/.nkeys/creds/op/ines/ines.creds"
	//_=userCreds

	var err error
	var pub *publisher.NatsPublisher

	if caCert == "" {
		pub, err = publisher.NewNatsPublisher(topic, server)
	} else {
		pub, err = publisher.NewNatsTlsPublisher(topic, server, caCert, userCreds)
	}
	if err != nil {
		log.Fatalf("Cannot connect to %s: %s\n", server, err)
		return
	}

	log.Printf("publisher connected to %s publish on %s\n", server, topic)

	message := &Message{}

	for {

		message.Next()
		data, err := message.Marshal()
		if err == nil {
			pub.Publish("message", data)
		}
		time.Sleep(delay)

	}

}
