#include .env

PROJECTNAME=$(shell basename "$(PWD)")

GOBASE=$(shell pwd)
#GOPATH=$(GOBASE)


# PID file will store the server process id when it's running on development mode
NATS_PID=/tmp/.$(PROJECTNAME)-nats-server.pid

# Make is verbose in Linux. Make it silent.
#MAKEFLAGS += --silent

hello :
	echo "Hello ${PROJECTNAME} at ${GOBASE}, GOPATH=${GOPATH}";


start-nats:
	@echo "start nats-server"
	nats-server 2>&1 & echo $$! > $(NATS_PID)
	@cat $(NATS_PID) | sed "/^/s/^/  \>  PID: /"

stop-nats:
	@-touch $(NATS_PID)
	@-kill `cat $(NATS_PID)` 2> /dev/null || true
	@-rm $(NATS_PID)


tests:
	#go test bitbucket.org/orangeparis/natsgateway



compile:
	echo "Compiling for darwin platform"
	GOOS=darwin GOARCH=amd64 go build -o build/packages/darwin_amd64/subscriber ./cmd/subscriber/subscriber.go
	GOOS=darwin GOARCH=amd64 go build -o build/packages/darwin_amd64/publisher ./cmd/publisher/publisher.go


	echo "Compiling for linux platform"
	GOOS=linux GOARCH=amd64 go build -o build/packages/linux_amd64/subscriber 	 ./cmd/subscriber/subscriber.go
	cp build/packages/linux_amd64/subscriber vagrant/files/subscriber
	GOOS=linux GOARCH=amd64 go build -o build/packages/linux_amd64/publisher 	 ./cmd/publisher/publisher.go
	cp build/packages/linux_amd64/publisher vagrant/files/publisher

	# copy certificates and conf
	cp pkg/cert.pem vagrant/files/cert.pem
	cp pkg/key.pem vagrant/files/key.pem
	cp pkg/server.conf vagrant/files/server.conf


