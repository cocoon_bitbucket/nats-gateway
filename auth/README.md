# auth #

nats authentication and permissions with nsc


# create credentials

## create the operator  ( op )

    ./nsc add operator -n op
    
    
    ./nsc describe operator op
    
    
## create account ( admin )

    ./nsc add account -n admin
    
    ./nsc describe account admin 
    
## create a user ( admin ) on  the admin account

    ./nsc add user -a admin -n admin --allow-pub ">" --allow-sub ">"
    
    ./nsc describe user admin -a admin
    
    
this create the user credentials files at

    ~/.nkeys/creds/op/admin/admin.creds
    
    
    
## create a generic account for ines , and a generic user ines

    ./nsc add account -n ines
    
    ./nsc add user -a ines -n ines --allow-pub ">" --allow-sub ">"
    
    
the user jwt token was generated at  

    ~/.nsc/nats/op/accounts/ines/users/ines.jwt
    

## export a public stream  sniffer.> from ines account

    ./nsc add export -a ines -n ines -s "sniffer.>" 
    
    ./nsc describe account ines
    
 ## import public stream sniffer.> into admin account from ines account
 
    ./nsc add import -a admin --remote-subject "sniffer.>" --src-account <Ines Account ID> 
    
    ./nsc describe account admin 
     
 
 

# Using NATS Account Server


get the nats-account-server

    go get github.com/nats-io/nats-account-server
    
start it

    nats-account-server -nsc ~/.nsc/nats/op
    
    nats-server -c server.conf
    
    
 
 
    